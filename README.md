# Bridging Microservice Boundaries With Apache Kafka and Debezium

A lab for Red Hat Summit 2019 focused on propagating data between microservices using Apache Kafka and Debezium, deployed to OpenShift via AMQ Streams.

Please refer to [the instructions](redhat-summit-2019/README.adoc) to get started with the lab.
